package com.novel.resource.qiniu.autoConfigurer;

import com.novel.resource.qiniu.config.QiniuConfig;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * qiniu 配置类
 *
 * @author novel
 * @date 2020/3/31
 */
@Configuration
@ConditionalOnClass({Auth.class})
@EnableConfigurationProperties(QiniuConfig.class)
@Slf4j
public class QiniuAutoConfiguration {
    private final QiniuConfig qiniuConfig;

    public QiniuAutoConfiguration(QiniuConfig qiniuConfig) {
        this.qiniuConfig = qiniuConfig;
    }

    @Bean
    public QiniuClientFactoryBean qiniuClientFactoryBean() {
        QiniuClientFactoryBean factoryBean = new QiniuClientFactoryBean(qiniuConfig);
        log.info("QiniuClientFactoryBean 初始化...");
        return factoryBean;
    }
}
