package com.novel.resource.cos.impl;


import com.novel.common.resource.ResourceType;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * Minio 注入控制
 *
 * @author novel
 * @date 2020/1/3
 */
public class MinioConditional implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        ResourceType type = context.getEnvironment().getProperty("resource.file-type", ResourceType.class);
        return ResourceType.MINIO.equals(type);
    }
}
