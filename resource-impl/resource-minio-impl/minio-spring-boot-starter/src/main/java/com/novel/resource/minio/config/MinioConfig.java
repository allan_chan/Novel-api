package com.novel.resource.minio.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * cos 配置
 *
 * @author novel
 * @date 2020/1/3
 */
@Data
@ConfigurationProperties(prefix = MinioConfig.MINIO_PREFIX)
public class MinioConfig {
    public static final String MINIO_PREFIX = "minio";
    /**
     * 地址
     */
    private String url;
    /**
     * 端口
     */
    private Integer port;
    /**
     * 账户名
     */
    private String accessKey;
    /**
     * 密钥
     */
    private String secretKey;
    /**
     * 是否使用https
     */
    private boolean secure;
    /**
     * 资源存放位置
     */
    private String bucketName;
}
