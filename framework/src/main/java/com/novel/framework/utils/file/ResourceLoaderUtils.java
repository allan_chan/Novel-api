package com.novel.framework.utils.file;

import cn.hutool.core.io.FileUtil;
import com.novel.common.resource.IResourceService;
import com.novel.common.utils.Assert;
import com.novel.framework.utils.config.Global;
import com.novel.framework.utils.spring.SpringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;

/**
 * 资源加载服务
 *
 * @author novel
 * @date 2019/6/5
 */
@Slf4j
public class ResourceLoaderUtils {
    private static IResourceService resourceService;
    private static ResourceLoader resourceLoader;

    static {
        if (Boolean.parseBoolean(Global.getConfig("resource.enable"))) {
            try {
                resourceService = SpringUtils.getBean(IResourceService.class);
            } catch (Exception e) {
                log.error("获取IResourceService服务失败,{}", e.getMessage());
            }
        }
        resourceLoader = new DefaultResourceLoader();
    }

    /**
     * 读取文件内容
     *
     * @param filePath 源文件地址
     * @return 文件的字节数组
     * @throws IOException IOException
     */
    public static byte[] readBytes(String filePath) throws IOException {
        byte[] bytes = null;
        try {
            Assert.isNotNull(filePath);
            String local = Global.getProfile() + filePath;
            log.debug("图片地址：" + local);
            Resource resource = resourceLoader.getResource("file:" + local);
            log.debug("本地是否存在缓存：" + resource.exists());
            if (resource.exists()) {
                bytes = FileUtils.getBytesByFile(resource.getFile());
            } else {
                if (Boolean.parseBoolean(Global.getConfig("resource.enable")) && resourceService != null) {
                    bytes = resourceService.readBytes(filePath);
                    if (Boolean.parseBoolean(Global.getConfig("resource.cache-enable"))) {
                        FileUtil.writeBytes(bytes, local);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return bytes;
    }

    /**
     * 文件下载
     *
     * @param filePath 源文件地址
     * @throws IOException
     */
    public static String download(String filePath) throws IOException {
        try {
            Assert.isNotNull(filePath);
            String local = Global.getProfile() + filePath;
            log.debug("图片地址：" + local);
            //查看本地是否存在缓存
            Resource resource = resourceLoader.getResource("file:" + local);
            //如果存在缓存，则直接返回，不存在，则下一步
            if (!resource.exists()) {
                //如果开启了文件服务器
                if (Boolean.parseBoolean(Global.getConfig("resource.enable")) && resourceService != null) {
                    //从文件服务器获取文件
                    resourceService.download(filePath, local);
                    return local;
                }
            }
        } catch (Exception e) {
            log.error(e.toString());
            e.printStackTrace();
            log.error("文件读取异常：" + filePath);
        }
        return null;
    }
}
