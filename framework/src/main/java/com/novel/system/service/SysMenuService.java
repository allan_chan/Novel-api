package com.novel.system.service;

import com.novel.system.domain.SysMenu;
import com.novel.system.domain.SysRole;
import com.novel.system.domain.SysUser;
import com.novel.system.domain.vo.MenuVo;
import com.novel.system.domain.vo.TreeData;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 菜单 业务层
 *
 * @author novel
 * @date 2019/12/20
 */
public interface SysMenuService {
    /**
     * 查询系统菜单列表
     *
     * @return 菜单列表
     */
    List<MenuVo> selectMenuList();

    /**
     * 根据用户ID查询菜单
     *
     * @param user 用户信息
     * @return 菜单列表
     */
    List<MenuVo> selectMenusByUser(SysUser user);

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    Set<String> selectPermsByUserId(Long userId);

    /**
     * 根据用户ID查询权限
     *
     * @param user
     * @return
     */
    Set<String> selectPermsByUser(SysUser user);

    /**
     * 查询所有菜单信息树，含按钮
     *
     * @return 菜单列表
     */
    List<TreeData> menuTreeData();

    /**
     * 查询所有菜单信息
     *
     * @return 菜单列表
     */
    List<SysMenu> menuTreeTableData();

    /**
     * 根据角色ID查询菜单
     *
     * @param role 角色对象
     * @return 菜单列表
     */
    Map<String, Object> roleMenuTreeData(SysRole role);

    /**
     * 查询所有菜单信息树，不含按钮
     *
     * @return 菜单列表
     */
    List<TreeData> menuTreeSelectData();

    /**
     * 修改保存菜单信息
     *
     * @param menu 菜单信息
     * @return 结果
     */
    boolean insertMenu(SysMenu menu);

    /**
     * 修改保存菜单信息
     *
     * @param menu 菜单信息
     * @return 结果
     */
    boolean updateMenu(SysMenu menu);

    /**
     * 根据菜单ID查询信息
     *
     * @param ids 菜单ID
     * @return 菜单信息
     */
    boolean deleteMenuByIds(Long[] ids);

    /**
     * 校验菜单名称是否唯一
     *
     * @param menu 菜单信息
     * @return 结果
     */
    String checkMenuNameUnique(SysMenu menu);
}
