package com.novel.service;

import com.novel.common.exception.job.TaskException;
import com.novel.domain.SysJob;
import org.quartz.SchedulerException;

import java.util.List;

/**
 * 定时任务调度信息信息 服务层
 *
 * @author novel
 * @date 2020/3/2
 */
public interface SysJobService {
    /**
     * 获取quartz调度器的计划任务
     *
     * @param job 调度信息
     * @return 调度任务集合
     */
    List<SysJob> selectJobList(SysJob job);

    /**
     * 通过调度任务ID查询调度信息
     *
     * @param id 调度任务ID
     * @return 调度任务对象信息
     */
    SysJob selectJobById(Long id);

    /**
     * 暂停任务
     *
     * @param job 调度信息
     * @return 结果
     * @throws SchedulerException 任务调度异常
     */
    boolean pauseJob(SysJob job) throws SchedulerException;

    /**
     * 恢复任务
     *
     * @param job 调度信息
     * @return 结果
     * @throws SchedulerException 任务调度异常
     */
    boolean resumeJob(SysJob job) throws SchedulerException;

    /**
     * 删除任务后，所对应的trigger也将被删除
     *
     * @param job 调度信息
     * @return 是否成功
     * @throws SchedulerException 任务调度异常
     */
    boolean deleteJob(SysJob job) throws SchedulerException;

    /**
     * 批量删除调度信息
     *
     * @param ids 需要删除的数据ID
     * @return 是否成功
     * @throws SchedulerException 任务调度异常
     */
    boolean deleteJobByIds(Long[] ids) throws SchedulerException;

    /**
     * 任务调度状态修改
     *
     * @param job 调度信息
     * @return 结果
     * @throws SchedulerException 任务调度异常
     */
    boolean changeStatus(SysJob job) throws SchedulerException;

    /**
     * 立即运行任务
     *
     * @param job 调度信息
     * @return 结果
     * @throws SchedulerException 任务调度异常
     */
    boolean run(SysJob job) throws SchedulerException;

    /**
     * 新增任务
     *
     * @param job 调度信息
     * @return 结果
     * @throws SchedulerException 任务调度异常
     * @throws TaskException      计划策略异常
     */
    boolean insertJob(SysJob job) throws SchedulerException, TaskException;

    /**
     * 更新任务
     *
     * @param job 调度信息
     * @return 结果
     * @throws SchedulerException 任务调度异常
     * @throws TaskException      计划策略异常
     */
    boolean updateJob(SysJob job) throws SchedulerException, TaskException;

    /**
     * 校验cron表达式是否有效
     *
     * @param cronExpression 表达式
     * @return 结果
     */
    boolean checkCronExpressionIsValid(String cronExpression);
}