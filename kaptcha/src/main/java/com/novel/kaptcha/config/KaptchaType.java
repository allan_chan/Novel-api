package com.novel.kaptcha.config;

/**
 * 验证码类型
 *
 * @author novel
 * @date 2019/12/2
 */
public enum KaptchaType {
    /**
     * 算数类型验证码
     */
    MATH,
    /**
     * 默认字符类型验证码
     */
    DEFAULT
}
